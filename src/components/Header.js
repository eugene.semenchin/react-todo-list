import React from 'react'

const Header = () => {
  return (
      <div className='header'>
         <h1 className='main__title'>Todo list</h1>
      </div>
  )
}

export default Header