import React from 'react'
import { FaCheck, FaPen, FaTrash } from 'react-icons/fa';

const TodoList = ({ todos, setTodos, setEditTodo }) => {
    const handleComplete = (todo) => {
        setTodos(
            todos.map((item) => {
                if (item.id === todo.id) {
                    return { ...item, completed: !item.compated };
                }
                return item;
            }));
    };

    const handleEdit = ({id}) => {
        const findTodo = todos.find((todo) => todo.id === id);
        setEditTodo(findTodo)
    };

    const handleDelete = ({ id }) => {
        setTodos(todos.filter((todo) => todo.id !== id));

    };

  return (
      <ul className='task__list'>
          {todos.map((todo) => (
              <li className='task__item' key={todo.id}>
                <div className='task__wrapper'>
                    <div className='remote__buttons'>
                        <button className='button-complete task__button' onClick={()=> handleComplete(todo)}>
                            <FaCheck style={{ color: "#ff8469", }}/>
                        </button>
                    </div>
                    <input readonly="readonly" type="text" value={todo.title} className={`list task__input ${todo.completed ? "complete" : "" }`}  onChange={(event) => event.preventDefault()}/> 
                </div>
                  <div className='task__wrapper'>
                    <div className='remote__buttons'>
                        <button className='button-edit task__button' onClick={() => handleEdit(todo)}>
                            <FaPen style={{color: "#ff8469",}} />
                        </button>
                        <button className='button-delete task__button' onClick={()=> handleDelete(todo)}>
                            <FaTrash style={{color: "#ff8469",}} />
                        </button> 
                    </div>
                </div>
            </li>
          ))}
      </ul>
  )
}

export default TodoList;